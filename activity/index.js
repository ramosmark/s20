const number = Number(prompt('Enter a number'));

for (let i = number; i >= 0; i--) {
	if (i <= 50) {
		break;
	}

	if (i % 10 === 0) {
		console.log('The number is divisible by 10. Skipping the number');
	}

	if (i % 5 === 0) {
		console.log(i);
	}
}

const word = 'supercalifragilisticexpialidocious';
let consonants = '';
const vowels = ['a', 'e', 'i', 'o', 'u'];

for (let i = 0; i < word.length; i++) {
	if (!vowels.includes(word[i])) {
		consonants += word[i];
	}
	1;
}

console.log(`The consonant letters are ${consonants}`);
